package karrel.com.codility;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

/**
 * Write a function:
 * 
 * class Solution { public int[] solution(int[] A, int K); }
 * 
 * that, given an array A consisting of N integers and an integer K, returns the
 * array A rotated K times.
 * 
 * For example, given
 * 
 * A = [3, 8, 9, 7, 6] K = 3 the function should return [9, 7, 6, 3, 8]. Three
 * rotations were made:
 * 
 * [3, 8, 9, 7, 6] -> [6, 3, 8, 9, 7] [6, 3, 8, 9, 7] -> [7, 6, 3, 8, 9] [7, 6,
 * 3, 8, 9] -> [9, 7, 6, 3, 8] For another example, given
 * 
 * A = [0, 0, 0] K = 1 the function should return [0, 0, 0]
 * 
 * Given
 * 
 * A = [1, 2, 3, 4] K = 4 the function should return [1, 2, 3, 4]
 * 
 * Assume that:
 * 
 * N and K are integers within the range [0..100]; each element of array A is an
 * integer within the range [−1,000..1,000]. In your solution, focus on
 * correctness. The performance of your solution will not be the focus of the
 * assessment.
 *
 * 
 * 
 */
public class CylicRotation {

	@Test
	public void testSolution() {

		int[] A = { 3, 8, 9, 7, 6 };
		int K = 3;
		int[] R = { 9, 7, 6, 3, 8 };
		assertThat(solution(A, K), is(R));
	}

	public int[] solution(int[] A, int K) {
		if (A.length == 0) {
			return A;
		}

		LinkedList<Integer> ll = new LinkedList<>();
		for (int a : A) {
			ll.add(a);
		}

		for (int i = 0; i < K; i++) {
			ll.add(0, ll.getLast());
			ll.remove(ll.size() - 1);

		}

		return toIntArray(ll);
	}

	private int[] toIntArray(List<Integer> list) {
		int[] ret = new int[list.size()];
		int i = 0;
		for (Integer e : list)
			ret[i++] = e;
		return ret;
	}

}
