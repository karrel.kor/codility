package karrel.com.codility;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class Uhyung1 {

	@Test
	public void testSolution() {
		assertThat(solution("011100"), is(7));
	}

	public int solution(String S) {
		int stepCnt = 0;

		int b = Integer.parseInt(S, 2);
		while (b != 0) {
			b = step(b);
			stepCnt++;
		}

		return stepCnt;
	}

	private int step(int b) {
		if (b % 2 == 0) {
			return b / 2;
		} else {
			return b - 1;
		}
	}
}
