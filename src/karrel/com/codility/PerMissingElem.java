package karrel.com.codility;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import javax.print.attribute.standard.PrinterLocation;

import org.junit.Test;

/**
 * An array A consisting of N different integers is given. The array contains
 * integers in the range [1..(N + 1)], which means that exactly one element is
 * missing.
 * 
 * Your goal is to find that missing element.
 * 
 * Write a function:
 * 
 * class Solution { public int solution(int[] A); }
 * 
 * that, given an array A, returns the value of the missing element.
 * 
 * For example, given array A such that:
 * 
 * A[0] = 2 A[1] = 3 A[2] = 1 A[3] = 5 the function should return 4, as it is
 * the missing element.
 * 
 * Write an efficient algorithm for the following assumptions:
 * 
 * N is an integer within the range [0..100,000]; the elements of A are all
 * distinct; each element of array A is an integer within the range [1..(N +
 * 1)].
 * 
 *
 * 
 */

public class PerMissingElem {

	@Test
	public void testSolution() {

		int[] A = { 1, 2, 3, 5 };
		assertThat(solution(A), is(4));
	}

	public int solution(int[] A) {
		// 1, 2, 3, 4, 5
		int N = A.length + 1;
		long sum = ((long) N * ((long) N + 1)) / 2;

		for (int a : A) {
			sum -= a;
		}

		return (int) sum;
	}
}
