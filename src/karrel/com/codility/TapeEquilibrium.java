package karrel.com.codility;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

/**
 * non-empty array A consisting of N integers is given. Array A represents
 * numbers on a tape.
 * 
 * Any integer P, such that 0 < P < N, splits this tape into two non-empty
 * parts: A[0], A[1], ..., A[P − 1] and A[P], A[P + 1], ..., A[N − 1].
 * 
 * The difference between the two parts is the value of: |(A[0] + A[1] + ... +
 * A[P − 1]) − (A[P] + A[P + 1] + ... + A[N − 1])|
 * 
 * In other words, it is the absolute difference between the sum of the first
 * part and the sum of the second part.
 * 
 * For example, consider array A such that:
 * 
 * A[0] = 3 A[1] = 1 A[2] = 2 A[3] = 4 A[4] = 3 We can split this tape in four
 * places:
 * 
 * P = 1, difference = |3 − 10| = 7 P = 2, difference = |4 − 9| = 5 P = 3,
 * difference = |6 − 7| = 1 P = 4, difference = |10 − 3| = 7 Write a function:
 * 
 * class Solution { public int solution(int[] A); }
 * 
 * that, given a non-empty array A of N integers, returns the minimal difference
 * that can be achieved.
 * 
 * For example, given:
 * 
 * A[0] = 3 A[1] = 1 A[2] = 2 A[3] = 4 A[4] = 3 the function should return 1, as
 * explained above.
 * 
 * Write an efficient algorithm for the following assumptions:
 * 
 * N is an integer within the range [2..100,000]; each element of array A is an
 * integer within the range [−1,000..1,000].
 *
 * 
 * 
 * N개의 정수로 구성된 비어 있지 않은 배열 A가 주어진다. 배열 A는 테이프의 숫자를 나타낸다.
 * 
 * 0 < P < N과 같은 정수 P는 이 테이프를 비어 있지 않은 두 부분으로 분할한다. A[0], A[1], ..., A[P - 1] 및
 * A[P], A[P + 1], ..., A[N - 1].
 * 
 * 두 부분의 차이는 |(A[0] + A[1] + ... + A[P - 1]) - (A[P] + A[P + 1] + ... + A[N -
 * 1])|
 * 
 * 즉, 첫 번째 부분의 합과 두 번째 부분의 합 사이의 절대차이다.
 * 
 * 예를 들어 다음과 같은 어레이 A를 고려하십시오.
 * 
 * A[0] = 3 A[1] = 1 A[2] = 2 A[3] = 4 A[4] = 3 이 테이프를 네 군데로 나눠서
 * 
 * P = 1, 차이 = |3 - 10| 7 P = 2, 차이 = |4 - 9| 5 P = 3, 차이 = |6 - 7| 1 P = 4, 차이
 * = |10 - 3| 7 함수 쓰기:
 * 
 * 클래스 솔루션 { public int 솔루션(int[] A), }
 * 
 * 비어 있지 않은 어레이 A의 N 정수를 사용하면 달성할 수 있는 최소 차이를 반환할 수 있다.
 * 
 * 예를 들면 다음과 같다.
 * 
 * A[0] = 3 A[1] = 1 A[2] = 2 A[3] = 4 A[4] = 3 위의 설명과 같이 함수는 1을 반환해야 한다.
 * 
 * 다음과 같은 가정에 대한 효율적인 알고리즘을 작성하십시오.
 * 
 * N은 [2..100,000] 범위의 정수 배열 A의 각 요소는 [-1,000] 범위의 정수다.1,000].
 * 
 * 
 */
public class TapeEquilibrium {

	@Test
	public void testSolution() {
		int[] A = { 3, 1, 2, 4, 3 };

		assertThat(solution(A), is(1));
	}

	public int solution(int[] A) {

		int front = 0;
		int back = 0;

		for (int i = 0; i < A.length; i++) {
			back += A[i];
		}

		int minDiff = Integer.MAX_VALUE;
		int p = 1;

		for (int i = 1; i < A.length; i++) {
			front += A[i - 1];
			back -= A[i - 1];
			p = i;

			minDiff = Math.min(minDiff, Math.abs(front - back));
		}

		return minDiff;
	}
}
