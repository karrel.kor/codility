
package karrel.com.codility;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

import org.junit.Test;

/**
 * People are waiting for and elevator in a hotel. The elevator has limited
 * capacity and you would like to analyse its movement The hotel has floors
 * numbered from 0 to M. the elevator has a maximum capacity of X people and a
 * weight limit of Y. There are N people gathered at the ground floor, standing
 * in a queue for the elevator. You are given every person's weight A and target
 * floor B
 * 
 * 
 * to the ground floor. This process is repeated until there are no more people
 * in the queue. The goal is to count the total number of times that the
 * elevator stops
 * 
 * for example, consider a hotel with floors numbered from 0 to M =5, with and
 * elevator with a maximum capacity of X = 2 people and a weight limit of Y =
 * 200, The weights A and target fllors B are
 * 
 * The elevator will take the first two passengers together, stop at the 2nd and
 * 3rd floors, then return to the ground floor. Then, it will take the last
 * passenger, stop at the 5th floor and return to the ground floor. In total,
 * the elevator will stop five times. Not that this number includes the last
 * stop at the ground floor.
 * 
 * that, given arrays A and B consisting of N integers, and numbers X, Y and M
 * as described above, returns the total number of times the elevator stops.
 * 
 * For example, given the above data, the function should return 5, as explained
 * above.
 * 
 * For example, given M = 3, X = 5, Y = 200 and the following arrays
 * 
 * the function should return 6, as the elevator will move in two atages: with
 * the first three people and then with the two remaining people.
 * 
 * 
 * @author karrel
 *
 */
public class Uhyung3 {

//	 			A[0] =  40    B[0] = 3		
//			    A[1] =  40    B[1] = 3
//			    A[2] = 100    B[2] = 2
//			    A[3] =  80    B[3] = 2
//			    A[4] =  20    B[4] = 3

	@Test
	public void testSolution() {
		int A[] = { 40, 40, 100, 80, 20 };
		int B[] = { 3, 3, 2, 2, 3 };

		int M = 3; // 층
		int X = 5; // 최대 사람
		int Y = 200; // 최대 무게

		System.out.println(solution(A, B, M, X, Y));

		int A2[] = { 60, 80, 40 };
		int B2[] = { 2, 3, 5 };

		M = 5; // 층
		X = 2; // 최대 사람
		Y = 200; // 최대 무게

		System.out.println("result : " + solution(A2, B2, M, X, Y));

//		assertThat(solution(A, B, M, X, Y), is(6));
	}

	public int solution(int[] A, int[] B, int M, int X, int Y) {

		// 1. 큐의 합이 X와 Y를 넘지 않는지 확인
		// 넘지 않으면 계속 넣는다.
		// 넘으면 엘레베이터 출발

		// 2. 첫번째 층에서 멈추고, 해당층에 내릴 사람들 모두 내림
		// 내릴 사람이 있는지 확인
		// 내릴 사람이 있으면 반복
		// 내릴 사람 없으면 0층으로 이동

		Queue<People> peoples = new LinkedList<>();

		for (int i = 0; i < A.length; i++) {
			if (B[i] <= M)
				peoples.add(new People(A[i], B[i]));
		}

		int count = 0;
		while (!peoples.isEmpty()) {
			count += calCount(peoples, M, X, Y);
//			System.out.println(count);
		}

		return count;
	}

	private int calCount(Queue<People> peoples, int M, int X, int Y) {
		int count = 0;

		LinkedList<People> elevatorPeople = new LinkedList<>();

		// 엘베이터 탈 수 있으면 태운다
		while (isAddable(elevatorPeople, peoples.peek(), X, Y)) {
			elevatorPeople.add(peoples.poll());
		}

		// 엘레베이터에 사람이 있으면
		while (!elevatorPeople.isEmpty()) {
			int floor = elevatorPeople.get(0).floor;

			// 엘레베이터에서 내린다
			removeFromElevator(elevatorPeople, floor);
			// 목표층 이동 카운트 증가
			++count;
		}

//		System.out.println("count : " + count);

		// 0층으로 이동
		return ++count;
	}

	private void removeFromElevator(LinkedList<People> elevatorPeople, int floor) {
		Iterator<People> iter = elevatorPeople.iterator();
		while (iter.hasNext()) {
			if (iter.next().floor == floor) {
				iter.remove();
			}
		}
	}

	private boolean isAddable(LinkedList<People> elevatorPeople, People addP, int X, int Y) {
		if (addP == null)
			return false;

		int weightSum = 0;
		int peopleSum = elevatorPeople.size() + 1;
		if (peopleSum > X)
			return false;

		for (People p : elevatorPeople) {
			weightSum += p.weight;
		}
		weightSum += addP.weight;

//		System.out.println("weightSum : " + weightSum);
//		System.out.println("peopleSum : " + peopleSum);

		if (peopleSum > X)
			return false;
		if (weightSum > Y)
			return false;
		return true;
	}

	class People {
		int weight;
		int floor;

		People(int weight, int floor) {
			this.weight = weight;
			this.floor = floor;
		}
	}
}
