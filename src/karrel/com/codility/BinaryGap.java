package karrel.com.codility;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class BinaryGap {

	@Test
	public void solutionTest() {
		assertThat(solution(1041), is(5));

	}

	public int solution(int N) {

		String binary = Integer.toBinaryString(N);

		int tmpCount = 0, result = 0;
		for (char b : binary.toCharArray()) {
			if (b == '1') {
				if (tmpCount > result) {
					result = tmpCount;
				}
				tmpCount = 0;
			} else {
				tmpCount++;
			}
		}

		System.out.println(binary);

		return result;
	}
}
