
package karrel.com.codility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;

/**
 * photo.jpg, Warsaw, 2013-09-05 14:08:15\njohn.png, London, 2015-06-20
 * 15:13:22\nmyFriends.png, Warsaw, 2013-09-05 14:07:13\nEiffel.jpg, Paris,
 * 2015-07-23 08:03:02\npisatower.jpg, Paris, 2015-07-22 23:59:59\nBOB.jpg,
 * London, 2015-08-05 00:02:03\nnotredame.png, Paris, 2015-09-01
 * 12:00:00\nme.jpg, Warsaw, 2013-09-06 15:40:22\na.png, Warsaw, 2016-02-13
 * 13:33:50\nb.jpg, Warsaw, 2016-01-02 15:12:22\nc.jpg, Warsaw, 2016-01-02
 * 14:34:30\nd.jpg, Warsaw, 2016-01-02 15:15:01\ne.png, Warsaw, 2016-01-02
 * 09:49:09\nf.png, Warsaw, 2016-01-02 10:55:32\ng.jpg, Warsaw, 2016-02-29
 * 22:13:11
 * 
 * @author karrel
 *
 */
public class Uhyung2 {

//	photo.jpg, Warsaw, 2013-09-05 14:08:15\njohn.png, London, 2015-06-20 15:13:22\nmyFriends.png, Warsaw, 2013-09-05 14:07:13\nEiffel.jpg, Paris, 2015-07-23 08:03:02\npisatower.jpg, Paris, 2015-07-22 23:59:59\nBOB.jpg, London, 2015-08-05 00:02:03\nnotredame.png, Paris, 2015-09-01 12:00:00\nme.jpg, Warsaw, 2013-09-06 15:40:22\na.png, Warsaw, 2016-02-13 13:33:50\nb.jpg, Warsaw, 2016-01-02 15:12:22\nc.jpg, Warsaw, 2016-01-02 14:34:30\nd.jpg, Warsaw, 2016-01-02 15:15:01\ne.png, Warsaw, 2016-01-02 09:49:09\nf.png, Warsaw, 2016-01-02 10:55:32\ng.jpg, Warsaw, 2016-02-29 22:13:11

	@Test
	public void testSolution() {
		System.out.println(solution(
				"photo.jpg, Warsaw, 2013-09-05 14:08:15\\njohn.png, London, 2015-06-20 15:13:22\\nmyFriends.png, Warsaw, 2013-09-05 14:07:13\\nEiffel.jpg, Paris, 2015-07-23 08:03:02\\npisatower.jpg, Paris, 2015-07-22 23:59:59\\nBOB.jpg, London, 2015-08-05 00:02:03\\nnotredame.png, Paris, 2015-09-01 12:00:00\\nme.jpg, Warsaw, 2013-09-06 15:40:22\\na.png, Warsaw, 2016-02-13 13:33:50\\nb.jpg, Warsaw, 2016-01-02 15:12:22\\nc.jpg, Warsaw, 2016-01-02 14:34:30\\nd.jpg, Warsaw, 2016-01-02 15:15:01\\ne.png, Warsaw, 2016-01-02 09:49:09\\nf.png, Warsaw, 2016-01-02 10:55:32\\ng.jpg, Warsaw, 2016-02-29 22:13:11"));

		System.out.println();
		
//		System.out.println("Warsaw02.jpg\nLondon1.png\nWarsaw01.png\nParis2.jpg\nParis1.jpg\nLondon2.jpg\nParis3.png\nWarsaw03.jpg\nWarsaw09.png\nWarsaw07.jpg\nWarsaw06.jpg\nWarsaw08.jpg\nWarsaw04.png\nWarsaw05.png\nWarsaw10.jpg");
		
		
	}
	

	public String solution(String S) {
		HashMap<String, Set<Photo>> photoMap = new HashMap<String, Set<Photo>>();
		List<Photo> photoList = new ArrayList<Photo>();
		String[] arrays = S.split("\\\\n");
		for (String s : arrays) {
			Photo p = new Photo(s); // photo 객체 생성
			photoList.add(p);

			Set<Photo> photoSet;
			if (photoMap.get(p.getCity_name()) == null) {
				photoSet = new TreeSet<Photo>();
			} else {
				photoSet = photoMap.get(p.getCity_name());
			}
			photoSet.add(p);
			photoMap.put(p.getCity_name(), photoSet); // 맵에 photo 그룹 넣기
		}

		for (Photo p : photoList) {
			Photo[] photoArray = photoMap.get(p.city_name).toArray(new Photo[photoMap.get(p.city_name).size()]);

			for (int i = 0; i < photoArray.length; i++) {
				if (p.equals(photoArray[i])) {
					if (photoArray.length < 10) {
						p.setIndex(String.format("%01d", i + 1));
					} else if (photoArray.length < 100) {
						p.setIndex(String.format("%02d", i + 1));
					}
					break;
				}
			}

		}

		
		String result = "";
		
		for (int i = 0; i < photoList.size(); i++) {
			if(i == 0) {
				result = photoList.get(i).getResult();	
			}else {
				result += "\n" + photoList.get(i).getResult();
			}
			
		}

		return result;
	}

	class Photo implements Comparable<Photo> {
		private String photoname;
		private String extension;
		private String city_name;
		private Date date;
		private String index;

		public Photo(String S) {
//			photo.jpg, Warsaw, 2013-09-05 14:08:15
			String[] arrays = S.split(", ");
			photoname = arrays[0];
			extension = getPhotoExtenstion(photoname);
			city_name = arrays[1];

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			try {
				date = sdf.parse(arrays[2]);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		public void setIndex(String index) {
			this.index = index;
		}

		public String getPhotoname() {
			return photoname;
		}

		public String getExtension() {
			return extension;
		}

		public String getCity_name() {
			return city_name;
		}

		public Date getDate() {
			return date;
		}

		public String getResult() {
			return String.format("%s%s.%s", city_name, index, extension);
		}

		@Override
		public boolean equals(Object obj) {

			return toString().equals(obj.toString());
		}

		@Override
		public String toString() {
			return String.format("photoname : %s, extension : %s, city_name : %s, date : %s", photoname, extension,
					city_name, date);
		}

		private String getPhotoExtenstion(String name) {
			if (name.endsWith("jpg")) {
				return "jpg";
			} else if (name.endsWith("png")) {
				return "png";
			} else if (name.endsWith("jpeg")) {
				return "jpeg";
			} else {
				throw new IllegalArgumentException("photo name extension is must be jpg, png or jpeg");
			}
		}

		@Override
		public int compareTo(Photo o) {
			if (date.getTime() > o.getDate().getTime())
				return 1;
			else if (date.getTime() < o.getDate().getTime())
				return -1;
			else
				return 0;
		}
	}

}
